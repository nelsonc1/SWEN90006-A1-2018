package swen90006.machine;

import java.util.Arrays;
import java.util.List;

import javax.print.DocFlavor.STRING;

import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  @Test public void aTest()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
    final int expected = 2;
    final int actual = 1 + 1;
    assertEquals(expected, actual);
  }

  @Test public void anotherTest()
  {
    List<String> list = new ArrayList<String>();
    list.add("a");
    list.add("b");

    //the assertTrue method is used to check whether something holds.
    assertTrue(list.contains("a"));
  }

  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //Test for valid instructions in any given instructions
  @Test public void ValidityTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    List<String> set = new ArrayList<>();

    set.add("ADD");
    set.add("SUB");
    set.add("MUL");
    set.add("DIV");
    set.add("RET");
    set.add("MOV");
    set.add("LDR");
    set.add("STR");
    set.add("JMP");
    set.add("JZ");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      for (String s : tokens){
        if (set.contains(s.toLowerCase())){
          System.out.print("This is a valid instruction\n");
        } else if (s.equals("")){
          System.out.print("This is a valid white space\n");
        } else {
          System.out.print("This is NOT a valid instruction\n");
          break;
        }
      }
    }
    return;
  }

  //Test for comments
  @Test public void CommentTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String i : lines){
      int index = i.indexOf(";");

      if (index == 0 ){
        System.out.print("This instruction is a comment\n");
      } else if (index == -1){
        System.out.print("There is NO comment in this instruction\n");
      } else {
        System.out.print("There is part instruction, part comment\n");
      }
    }
  }

  //Test for white spaces
  @Test public void SpaceTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String i : lines){
      String s = i.replaceAll("\\s+","");
      if (s.isEmpty()){
        System.out.print("This is a blank space\n");
      } else {
        System.out.print("This is NOT a blank space\n");
      }
    }
  }

  @Test public void EmptyTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String i : lines){
      if (i.isEmpty()){
        System.out.print("There is NO instruction here\n");
      } else {
        System.out.print("There is an instruction here\n");
      }
    }
  }

  // Test for operator instructions (ADD, SUB, MUL, DIV)
  @Test public void OperatorTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    List<String> operators = new ArrayList<>();

    operators.add("ADD");
    operators.add("SUB");
    operators.add("MUL");
    operators.add("DIV");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      if (operators.contains(tokens[0])){
        if (tokens.length == 4){
          for (int i=1; i < 4; i++){
            int r = Integer.parseInt(tokens[i].replace("R",""));

            if (r < 0 || r > 31 ){
              System.out.print("Invalid register\n");
              return;
            }

            System.out.print("Operator instruction is valid\n");
          }
        } else if (tokens.length > 4){
          System.out.print("Invalid number of arguments\n");
        }
      } else {
        System.out.print("Operators: Instruction is NOT an operator\n");
        break;
      }
    }
  }

  // Test for RET instruction
  @Test public void RetTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      if (tokens.length == 2){
        if (tokens[0] != "RET"){
          System.out.print("This is NOT a RET instruction\n");
        } else{
          int r = Integer.parseInt(tokens[1].replace("R",""));

          if (r < 0 || r > 31 ){
            System.out.print("Invalid register\n");
            return;
          }
          System.out.print("RET instruction is valid\n");
        }
      } else {
        System.out.print("RET: Invalid number of arguments\n");
        break;
      }
    }
  }

  // Test fo MOV instruction
  @Test public void MovTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      if (tokens.length == 4){
        if (tokens[0] != "MOV"){
          System.out.print("This is NOT a MOV instruction\n");
        } else{
          int r = Integer.parseInt(tokens[1].replace("R",""));

          if (r < 0 || r > 31 ){
            System.out.print("Invalid register\n");
            return;
          }

          int val = Integer.parseInt(tokens[2]);

          if (val < -65536 || val > 65535) {
            System.out.print("Invalid value\n");
            return;
          }
          System.out.print("MOV instruction is valid\n");
        }
      } else {
        System.out.print("MOV: Invalid number of arguments\n");
        break;
      }
    }
    return;
  }

  // Test for LDR Instruction
  @Test public void LdrTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      if (tokens.length == 4){
        if (tokens[0] != "LDR"){
          System.out.print("This is NOT a LDR instruction\n");
        } else{
          int r = Integer.parseInt(tokens[1].replace("R", ""));

          if (r < 0 || r > 31 ){
            System.out.print("Invalid register\n");
            return;
          }

          int r2 = Integer.parseInt(tokens[2].replace("R", ""));

          if (r2 < 0 || r2 > 31 ){
            System.out.print("Invalid register\n");
            return;
          }

          int val = Integer.parseInt(tokens[3]);

          if (val < -65536 || val > 65535) {
            System.out.print("Invalid value\n");
            return;
          }
          System.out.print("LDR instruction is valid\n");
        }
      } else {
        System.out.print("LDR: Invalid number of arguments\n");
        break;
      }
    }
    return;
  }

  // Test fo STR instruction
  @Test public void StrTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      if (tokens.length == 4){
        if (tokens[0] != "STR"){
          System.out.print("This is NOT a LDR instruction\n");
        } else{
          int r = Integer.parseInt(tokens[1].replace("R",""));

          if (r < 0 || r > 31 ){
            System.out.print("Invalid register\n");
            return;
          }

          int val = Integer.parseInt(tokens[2]);

          if (val < -65536 || val > 65535) {
            System.out.print("Invalid value\n");
            return;
          }

          int r2 = Integer.parseInt(tokens[3].replace("R",""));

          if (r2 < 0 || r2 > 31 ){
            System.out.print("Invalid register\n");
            return;
          }

          System.out.print("STR instruction is valid\n");
        }
      } else {
        System.out.print("STR: Invalid number of arguments\n");
        break;
      }
    }
    return;
  }

  // Test for JMP Instruction
  @Test public void JmpTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      if (tokens[0] == "JMP"){
        if (tokens.length == 2){
          int val = Integer.parseInt(tokens[1]);

          if (val < -65536 || val > 65535) {
            System.out.print("Invalid value\n");
            return;
          }

          //System.out.print("JMP instruction is valid\n");
        } else {
          System.out.print("JMP: Invalid number of arguments\n");
          break;
        }
      }
    }
    return;
  }

  // Test fo JZ Instruction
  @Test public void JzTest()
  {
    final List<String> lines = readInstructions("examples/array.s");

    for (String in : lines){
      in = in.split(";")[0]; // Remove comments
      String[] tokens = in.split(" ");
      if (tokens[0] == "JZ"){
        if (tokens.length == 3){
            int r = Integer.parseInt(tokens[1].replace("R",""));

            if (r < 0 || r > 31 ){
              System.out.print("Invalid register\n");
              return;
            }

            int val = Integer.parseInt(tokens[2]);

            if (val < -65536 || val > 65535) {
              System.out.print("Invalid value\n");
              return;
            }

            //System.out.print("JZ instruction is valid\n");
          } else {
            System.out.print("JZ: Invalid number of arguments\n");
            break;
        }
      }
    }

    return;
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
